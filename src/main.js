/**
 * 描述：入口文件
 */
import Vue from 'vue';
import Meta from 'vue-meta'
import router from './router/';
import Element from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import '@/styles/element-theme/index.css';
import iView from 'iview';
import 'iview/dist/styles/iview.css';
import '@/styles/iview-theme/dist/iview.css';
import store from './store/';
import core from '@/js/core';       // 公共指令及filter

Vue.config.productionTip = false;
Vue.use(Element);
Vue.use(core);
Vue.use(iView);
Vue.use(Meta);

/* eslint-disable no-new */
new Vue({
	router,
	store
}).$mount('#app');
