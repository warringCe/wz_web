/**
 * 描述：路由文件
 */
import App from '../../App';

// 404
const _404 = r => require.ensure([], () => r(require('../../page/404/404')), '_404');

// web 前端页面
const web = r => require.ensure([], () => r(require('../../page/web/web')), 'web');  // web框架vue
const webMain = r => require.ensure([], () => r(require('../../page/web/page/main')), 'webMain');  // 主内容vue
const webHome = r => require.ensure([], () => r(require('../../page/web/page/home/home')), 'webHome');
const webLogin = r => require.ensure([], () => r(require('../../page/web/login/login')), 'webLogin');
const userSelect = r => require.ensure([], () => r(require('../../page/web/userSelect')), 'userSelect'); // 用户选择
const userSelectRegister = r => require.ensure([], () => r(require('../../page/web/userSelectRegister')), 'userSelectRegister'); // 用户选择
const webRegister = r => require.ensure([], () => r(require('../../page/web/login/register')), 'webRegister');
const webPerfect = r => require.ensure([], () => r(require('../../page/web/login/perfect')), 'webPerfect');
const webMap = r => require.ensure([], () => r(require('../../page/web/page/map/map')), 'webMap');
const companyDetail = r => require.ensure([], () => r(require('../../page/web/page/companyDetail/index')), 'companyDetail'); // 公司详情
const searchJobs = r => require.ensure([], () => r(require('../../page/web/page/searchJobsFlow/search')), 'searchJobs'); // 搜索职位
const jobDetail = r => require.ensure([], () => r(require('../../page/web/page/searchJobsFlow/jobDetail')), 'jobDetail'); // 搜索职位
const userCenterIndex = r => require.ensure([], () => r(require('../../page/web/page/userCenter/index')), 'userCenterIndex'); // 个人中心
const userResumeCenter = r => require.ensure([], () => r(require('../../page/web/page/userCenter/page/resumeCenter')), 'userResumeCenter'); // 个人中心 - 我的简历
const userWhoHasSeen = r => require.ensure([], () => r(require('../../page/web/page/userCenter/page/whoHasSeen')), 'userWhoHasSeen'); // 个人中心 - 谁看过我
const userMyApplication = r => require.ensure([], () => r(require('../../page/web/page/userCenter/page/myApplication')), 'userMyApplication'); // 个人中心 - 我的申请
const userSecurityCenter = r => require.ensure([], () => r(require('../../page/web/page/userCenter/page/securityCenter')), 'userSecurityCenter'); // 个人中心 - 安全中心
const userPersonnelLetter = r => require.ensure([], () => r(require('../../page/web/page/userCenter/page/personnelLetter')), 'userPersonnelLetter'); // 个人中心 - 人事来信
const userMore = r => require.ensure([], () => r(require('../../page/web/page/userCenter/page/more')), 'userMore'); // 个人中心 - 更多
const systemInfo = r => require.ensure([], () => r(require('../../page/web/page/userCenter/page/systemInfo')), 'systemInfo'); // 个人中心 - 系统消息
const shieldingCompany = r => require.ensure([], () => r(require('../../page/web/page/userCenter/page/shieldingCompany')), 'shieldingCompany'); // 个人中心 - 屏蔽公司
const calculatingPrice = r => require.ensure([], () => r(require('../../page/web/page/userCenter/page/calculatingPrice')), 'calculatingPrice'); // 个人中心 - 计算身价
const exportResume = r => require.ensure([], () => r(require('../../page/web/page/userCenter/page/export')), 'exportResume'); // 个人中心 - 计算身价
// 广告图
const ads01 = r => require.ensure([], () => r(require('../../page/web/page/ads/ads01')), 'ads01');
const ads02 = r => require.ensure([], () => r(require('../../page/web/page/ads/ads02')), 'ads02');
const ads03 = r => require.ensure([], () => r(require('../../page/web/page/ads/ads03')), 'ads03');
const ads04 = r => require.ensure([], () => r(require('../../page/web/page/ads/ads04')), 'ads04');

// mge 管理系統
const mge = r => require.ensure([], () => r(require('../../page/mge/mge')), 'mge');  // mge框架vue
const mgeMain = r => require.ensure([], () => r(require('../../page/mge/page/main')), 'mgeMain');  // 主内容vue
const mgeLogin = r => require.ensure([], () => r(require('../../page/mge/login/login')), 'mgeLogin'); // 登录
const mgeHome = r => require.ensure([], () => r(require('../../page/mge/page/home/home')), 'mgeHome'); // 首页
const mgeCompany = r => require.ensure([], () => r(require('../../page/mge/page/company/company')), 'mgeCompany'); // 企业管理
const mgeUser = r => require.ensure([], () => r(require('../../page/mge/page/user/user')), 'mgeUser'); // 用户管理
const mgeActivity = r => require.ensure([], () => r(require('../../page/mge/page/activity/activity')), 'mgeActivity'); // 活动管理
const mgeCity = r => require.ensure([], () => r(require('../../page/mge/page/city/city')), 'mgeCity'); // 城市管理

// company 企业管理
const company = r => require.ensure([], () => r(require('../../page/company/company')), 'company');  // company框架vue
const companyMain = r => require.ensure([], () => r(require('../../page/company/page/main')), 'companyMain');  // 主内容vue
const companyLogin = r => require.ensure([], () => r(require('../../page/company/login/login')), 'companyLogin'); // 登录
const companyRegister = r => require.ensure([], () => r(require('../../page/company/login/register')), 'companyRegister'); // 注册
const companyHome = r => require.ensure([], () => r(require('../../page/company/page/home/home')), 'companyHome'); // 首页
const companyResume = r => require.ensure([], () => r(require('../../page/company/page/resume/index')), 'companyResume'); // 简历管理
const companyStation = r => require.ensure([], () => r(require('../../page/company/page/station/index')), 'companyStation'); // 岗位管理
const companyStationAdd = r => require.ensure([], () => r(require('../../page/company/page/station/add')), 'companyStationAdd'); // 岗位管理-新增
const companyTalentPool = r => require.ensure([], () => r(require('../../page/company/page/talentPool/index')), 'companyTalentPool'); // 人才库
const companyTalentPoolDetail = r => require.ensure([], () => r(require('../../page/company/page/talentPool/detail')), 'companyTalentPoolDetail'); // 人才库-详情
const companyCandidate = r => require.ensure([], () => r(require('../../page/company/page/candidate/index')), 'companyCandidate'); // 候选库
const companyJobInvitation = r => require.ensure([], () => r(require('../../page/company/page/jobInvitation/index')), 'companyJobInvitation'); // 职位邀请
const companyCenter = r => require.ensure([], () => r(require('../../page/company/page/companyCenter/index')), 'companyCenter'); // 简历中心
const companySubaccount = r => require.ensure([], () => r(require('../../page/company/page/subaccount/index')), 'companySubaccount'); // 子账号管理
const companyEnterpriseInfo = r => require.ensure([], () => r(require('../../page/company/page/enterpriseInfo/index')), 'companyEnterpriseInfo'); // 企业管理
const companyActivities = r => require.ensure([], () => r(require('../../page/company/page/activities/index')), 'companyActivities'); // 活动申请
const companyMyAccount = r => require.ensure([], () => r(require('../../page/company/page/myAccount/index')), 'companyMyAccount'); // 我的信息
const companyExportResume = r => require.ensure([], () => r(require('../../page/company/page/export/export')), 'exportResume'); // 我的信息

export default [
  // 未匹配到则404页面
  {
    path: '*',
    component: _404
  },
  {
    path: '/',
    component: App,
    children: [
      // -----------------------------------------------web---------------------------------------------------
      {
        path: '',
        redirect: 'home'
      },
      {
        path: '/',
        component: web,
        children: [
          {
            path: 'userSelect', // 用户选择登录
            component: userSelect
          },
          {
            path: 'userSelectRegister', // 用户选择注册
            component: userSelectRegister
          },
          {
            path: 'exportResume', // 预览简历
            component: exportResume
          },
          {
            path: 'login', // 登录
            component: webLogin
          },
          {
            path: 'register', // 注册
            component: webRegister
          },
          {
            path: 'perfect', // 完善注册
            component: webPerfect
          },
          {
            path: 'company/login', // 企业登录
            component: companyLogin
          },
          {
            path: 'company/register', // 企业注册
            component: companyRegister
          },
          {
            path: '/',
            component: webMain,
            children: [
              {
                path: 'home', // 首页
                component: webHome
              },
              {
                path: 'map', // 地图
                component: webMap
              },
              {
                path: 'searchJobs', // 搜索职位
                component: searchJobs
              },
              {
                path: 'jobDetail', // 职位详情
                component: jobDetail
              },
              {
                path: 'companyDetail', // 公司详情
                component: companyDetail
              },
              {
                path: 'userCenter', // 个人中心
                component: userCenterIndex,
                children: [
                  {
                    path: 'resumeCenter', // 我的简历
                    component: userResumeCenter
                  },
                  {
                    path: 'whoHasSeen', // 谁看过我
                    component: userWhoHasSeen
                  },
                  {
                    path: 'myApplication', // 我的申请
                    component: userMyApplication
                  },
                  {
                    path: 'securityCenter', // 安全中心
                    component: userSecurityCenter
                  },
                  {
                    path: 'personnelLetter', // 人事来信
                    component: userPersonnelLetter
                  },
                  {
                    path: 'systemInfo', // 系统消息
                    component: systemInfo
                  },
                  {
                    path: 'shieldingCompany', // 屏蔽公司
                    component: shieldingCompany
                  },
                  {
                    path: 'calculatingPrice', // 身价计算
                    component: calculatingPrice
                  },
                  {
                    path: 'more', // 更多
                    component: userMore
                  }
                ]
              },
              {
                path: 'ads01', // 广告
                component: ads01
              },
              {
                path: 'ads02', // 广告
                component: ads02
              },
              {
                path: 'ads03', // 广告
                component: ads03
              },
              {
                path: 'ads04', // 广告
                component: ads04
              },
              // -----------------------------------------------企业管理---------------------------------------------------
              {
                path: 'company',
                component: company,
                children: [
                  {
                    path: '',
                    redirect: 'login'
                  },
                  {
                    path: 'main',
                    component: companyMain,
                    children: [
                      {
                        path: 'home', // 首页
                        component: companyHome
                      },
                      {
                        path: 'resume', // 简历管理
                        component: companyResume
                      },
                      {
                        path: 'station', // 岗位管理
                        component: companyStation
                      },
                      {
                          path: 'station/add', // 岗位管理-新增
                        component: companyStationAdd
                      },
                      {
                        path: 'talentPool', // 人才库
                        component: companyTalentPool
                      },
                      {
                        path: 'talentPool/detail', // 人才库-详情
                        component: companyTalentPoolDetail
                      },
                      {
                        path: 'candidate', // 候选库
                        component: companyCandidate
                      },
                      {
                        path: 'jobInvitation', // 职位邀请
                        component: companyJobInvitation
                      },
                      {
                        path: 'companyCenter', // 简历中心
                        component: companyCenter
                      },
                      {
                        path: 'subaccount', // 子账号管理
                        component: companySubaccount
                      },
                      {
                        path: 'enterpriseInfo', // 企业管理
                        component: companyEnterpriseInfo
                      },
                      {
                        path: 'activities', // 活动申请
                        component: companyActivities
                      },
                      {
                        path: 'myAccount', // 我的信息
                        component: companyMyAccount
                      },
                      {
                        path: 'companyExportResume', // 简历预览
                        component: companyExportResume
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      },
      // -----------------------------------------------后台管理---------------------------------------------------
      {
        path: 'mge',
        component: mge,
        children: [
          {
            path: '',
            redirect: 'login'
          },
          {
            path: 'login', // 登录
            component: mgeLogin
          },
          {
            path: 'main',
            component: mgeMain,
            children: [
              {
                path: 'home', // 首页
                component: mgeHome
              },
              {
                path: 'company', // 企业管理
                component: mgeCompany
              },
              {
                path: 'user', // 用户管理
                component: mgeUser
              },
              {
                path: 'activity', // 活动管理
                component: mgeActivity
              },
              {
                path: 'city', // 城市管理
                component: mgeCity
              }
            ]
          }
        ]
      }
    ]
  }
];
