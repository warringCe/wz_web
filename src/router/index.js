/**
 * 描述：路由文件
 */
import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from './src/router.js';
import iView from 'iview';

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes,
    strict: process.env.NODE_ENV !== 'production'
});

// 路由未登录拦截
router.beforeEach((to, from, next) => {
  let pathArray = to.path.split('/');
  let mgeToken = Vue._hyTool.getStore('mgeLoginInfo');
  let companyToken = Vue._hyTool.getStore('companyLoginInfo');
  let webToken = Vue._hyTool.getStore('webLoginInfo');
  if (!mgeToken && pathArray[1] === 'mge' && to.path !== '/mge/login') {
    next({path: '/mge/login'});
  } else if (!companyToken && pathArray[1] === 'company' && to.path !== '/company/login') {
    if (to.path === '/company/register') {
      next();
    } else {
      next({path: '/company/login'});
    }
  } else if (!webToken && to.path.indexOf('/userCenter') !== -1) {
    next({path: '/login'});
  }
  iView.LoadingBar.start();
  next();
});

router.afterEach(route => {
  window.scrollTo(0,0);
  iView.LoadingBar.finish();
});

export default router;
