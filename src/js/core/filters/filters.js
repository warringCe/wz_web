/**
 *  描述：Global filters js
 */
import tool from '@/js/core/utils/tool';

// 待安装
const install = Vue => {
  // 时间filter
  Vue.filter('formatDate', function (value) {
      if (!value) return '至今';
      let val = new Date(Date.parse(value.replace(/-/g,"/")));
      return tool.DateFormat(val, 'yyyy-MM-dd');
  });
  // 时间filter
  Vue.filter('formatDate2', function (value) {
    if (!value) return '';
    let val = new Date(Date.parse(value.replace(/-/g,"/")));
    return tool.DateFormat(val, 'yyyy-MM-dd');
  });
  // 时间filte2
  Vue.filter('formatDate3', function (value) {
    if (!value) return '';
    let val = new Date(Date.parse(value.replace(/-/g,"/")));
    return tool.DateFormat(val, 'yyyy/MM');
  });
  // 截取时间filter
  Vue.filter('substringDate', function (value) {
    if (!value) return '';
    return value.substring(0,10);
  });

  // 时间filter
  Vue.filter('formatTime', function (value) {
      if (!value) return '';
      let val = new Date(+value);
      return tool.DateFormat(val, 'yyyy-MM-dd hh:mm:ss');
  });

  // 性别
  Vue.filter('sexType', function (value) {
    switch (value) {
      case 0:
        return '未指定';
      case 1:
        return '男';
      case 2:
        return '女';
    }
  });

  // 简历刷新状态
  Vue.filter('visibleFilter', function (value) {
     if (value) {
       return '保密';
     } else {
       return '公开';
     }
  });

  // 职位状态
  Vue.filter('jopStatus', function (value) {
    switch (value) {
      case 0:
        return '未发布';
      case 1:
        return '已发布';
      case 2:
        return '已下线';
    }
  });

  // 工作经验要求
  Vue.filter('experience', function (value) {
    switch (value) {
      case 0:
        return '不限';
      case 1:
        return '1－3年';
      case 2:
        return '3－5年';
      case 3:
        return '5－10年';
      case 10:
        return '10年以上';
    }
  });

  // 工作经验要求 - 个人中心
  Vue.filter('userExperience', function (value) {
    switch (value) {
      case 0:
        return '一年以下';
      case 1:
        return '1－3年';
      case 2:
        return '3－5年';
      case 3:
        return '5－10年';
      case 10:
        return '10年以上';
    }
  });

  // 工作经验要求 - 个人中心
  Vue.filter('arrivalTime', function (value) {
    switch (value) {
      case 0:
        return '随时';
      case 1:
        return '一周';
      case 2:
        return '两周';
      case 3:
        return '一个月';
      case 4:
        return '两个月';
      case 5:
        return '三个月';
      case 6:
        return '两个月';
    }
  });

  // 年龄
  Vue.filter('jsGetAge', function (value) {
    if (!value) return '未知';
    let val = new Date(Date.parse(value.replace(/-/g,"/")));
    let d = new Date();
    var nowYear = d.getFullYear();
    var agoYear = val.getFullYear();
    return nowYear - agoYear;
  });

  // 最高学历
  Vue.filter('education', function (value) {
    switch (value) {
      case 0:
        return '中专(高中)';
      case 1:
        return '大专';
      case 2:
        return '本科';
      case 3:
        return '硕士';
      case 4:
        return '博士';
      case 5:
        return '其它';
    }
  });

  // 帐号是否禁用
  Vue.filter('bannedStatus', function (value) {
    if (value) {
      return '是';
    } else {
      return '否';
    }
  });

  // 申请状态
  Vue.filter('applyStatus', function (value) {
    switch (value) {
      case 1:
        return '已发出申请';
      case 2:
        return '已查阅';
      case 3:
        return '已列入企业候选人才库';
      case 4:
        return '已发出邀约';
      case 5:
        return '已接受邀约';
      case 6:
        return '已入职';
    }
  });

  // 企业认证状态
  Vue.filter('verifyStatus', function (value) {
    switch (value) {
      case 0:
        return '未认证';
      case 1:
        return '认证审核中';
      case 2:
        return '认证成功';
      case 3:
        return '认证失败';
    }
  });

  // 企业审核状态
  Vue.filter('examineState', function (value) {
    switch (value) {
      case 0:
        return '未审核';
      case 1:
        return '审核成功';
      case 2:
        return '审核失败';
    }
  });

  // 申请状态
  Vue.filter('inviteStatus', function (value) {
    switch (value) {
      case 1:
        return '已发出邀约';
      case 2:
        return '已接受邀约';
      case 3:
        return '已入职';
      case 4:
        return '未入职';
      case 5:
        return '已拒绝邀约';
    }
  });
// ------------------------------------------- 个人中心 --------------------------------------------

  // 信息完整度
  Vue.filter('userInfoPoint', function (value) {
    switch (value) {
      case 0:
        return '仅注册帐户';
      case 1:
        return '已填写基本信息';
      case 2:
        return '已录入求职意向';
      case 3:
        return '已填写简历';
    }
  });
  // 求职状态
  Vue.filter('intentStatus', function (value) {
    switch (value) {
      case 0:
        return '积极找工作';
      case 1:
        return '随便看看';
      case 2:
        return '暂时不换工作';
    }
  });
  // 求职状态
  Vue.filter('intentType', function (value) {
    switch (value) {
      case 0:
        return '全职';
      case 1:
        return '兼职';
      case 2:
        return '实习';
    }
  });
  // 信息完整度
  Vue.filter('profileIntegrity', function (value) {
    switch (value) {
      case 0:
        return '仅注册帐户';
      case 1:
        return '已填写基本信息';
      case 2:
        return '已录入求职意向';
      case 3:
        return '已填写简历';
    }
  });
  // 邀约状态 - 用户
  Vue.filter('invitationState', function (value) {
    switch (value) {
      case 1:
        return '已发出邀约';
      case 2:
        return '已接受邀约';
      case 3:
        return '已入职';
      case 5:
        return '已拒绝';
    }
  });
  // 邀约状态 - 企业
  Vue.filter('resumeState', function (value) {
    switch (value) {
      case 1:
        return '已发出申请';
      case 2:
        return '已查阅';
      case 3:
        return '已列入企业人才库';
      case 4:
        return '已发出邀约';
      case 5:
        return '已入职';
      case 6:
        return '未入职';
      case 7:
        return '已拒绝';
    }
  });
  // 子账号管理 - 角色类型
  Vue.filter('subaccountType', function (value) {
    switch (value) {
      case 'companyRoot':
        return '管理员';
      case 'recruiter':
        return '子账号';
    }
  });
  // 城市
  Vue.filter('cityAndType', function (value) {
    if (!value) return '';
    let result = tool.getStore('cityData');
    let cityData = JSON.parse(result)
    let cityObject = cityData.find(item => item.id === value);
    let str = '';
    if (cityObject) str = cityObject.name;
    return str;
  });
  // 省
  Vue.filter('provinceFilter', function (value) {
    if (!value) return '';
    let result = tool.getStore('cityData');
    let cityData = JSON.parse(result)
    let cityObject = cityData.find(item => item.id === value);
    let result1 = tool.getStore('provinceData');
    let provinceData = JSON.parse(result1);
    let provinceObject = provinceData.find(item => item.id === cityObject.parentId);
    let str = '';
    if (provinceObject) str = provinceObject.name;
    return str;
  });
  // 职务过滤
  Vue.filter('positionFilter', function (value) {
    if (!value) return '';
    let result = tool.getStore('jobListData');
    let jobListData = JSON.parse(result);
    let jobListDataObject = jobListData.find(item => item.id === value);
    let str = '';
    if (jobListDataObject) str = jobListDataObject.label;
    return str;
  });
  // 发布日期过滤
  Vue.filter('releaseDate', function (value) {
    if (!value) return '';
    let day1 = new Date(value);
    let day2 = new Date();
    return parseInt((day2 - day1) / (1000 * 60 * 60 * 24)) + '天前';
  });
  // 行业过滤
  Vue.filter('industryFilter', function (value) {
    if (!value) return '';
    let result = tool.getStore('industryListData');
    let industryListData = JSON.parse(result);
    let listDataObject = industryListData.find(item => item.id === value);
    let str = '';
    if (listDataObject) str = listDataObject.label;
    return str;
  });
  // 公司规模过滤
  Vue.filter('companyScaleFilter', function (value) {
    if (!value) return '';
    let result = tool.getStore('companyScaleData');
    let companyScaleData = JSON.parse(result);
    let listDataObject = companyScaleData.find(item => item.id === value);
    let str = '';
    if (listDataObject) str = listDataObject.label;
    return str;
  });
  // 企业融资过滤
  Vue.filter('capitalStageFilter', function (value) {
    if (!value) return '';
    let result = tool.getStore('capitalStageData');
    let capitalStageData = JSON.parse(result);
    let listDataObject = capitalStageData.find(item => item.id === value);
    let str = '';
    if (listDataObject) str = listDataObject.label;
    return str;
  });
  // 企业人数过滤
  Vue.filter('companyScaleSize', function (value) {
    if (!value) return '';
    let result = tool.getStore('companyScaleListData');
    let capitalStageData = JSON.parse(result);
    let listDataObject = capitalStageData.find(item => item.id === value);
    let str = '';
    if (listDataObject) str = listDataObject.label;
    return str;
  });
  // 小时取余天
  Vue.filter('applicantProcessTime', function (value) {
    return  Math.ceil(value / 24);
  });
  // 薪资
  Vue.filter('salaryState', function (value) {
    switch (value) {
      case 0:
        return '面议';
      case 1:
        return '1000-2000';
      case 2:
        return '2000-3000';
      case 3:
        return '3000-4000';
      case 4:
        return '4000-5000';
      case 5:
        return '5000-8000';
      case 6:
        return '8000-10000';
      case 7:
        return '10000-15000';
      case 8:
        return '15000-20000';
      case 9:
        return '20000-30000';
      case 10:
        return '30000-50000';
      case 11:
        return '50000-100000';
      case 12:
        return '100000以上';
    }
  });
  // 薪资
  Vue.filter('demandState', function (value) {
    switch (value) {
      case 0:
        return '若干';
      case 1:
        return '1人';
      case 2:
        return '2人';
      case 3:
        return '3人';
      case 4:
        return '4人';
      case 5:
        return '5人';
      case 6:
        return '6人';
      case 7:
        return '7人';
      case 8:
        return '8人';
      case 9:
        return '9人';
      case 10:
        return '10人';
      case 11:
        return '其他';
    }
  });
};

export default install;
