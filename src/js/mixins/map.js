/**
 * 描述：map
 */
 import AMap from 'AMap';
 import {Notice} from 'iview';

 export default {
     data () {
         return {
             map: null,
             viewport: null
         };
     },
     methods: {
        _initMap (id, zoom) {
            return new Promise((resolve, reject) => {
                try {
                    this.map = new AMap.Map(id, {
                        resizeEnable: true,
                        zoom: zoom
                    });
                    resolve(true);
                } catch (e) {
                    Notice.error({
                      title: '地图初始化失败，请稍后再试!'
                    });
                    reject('地图初始化失败，请稍后再试!');
                }
            });
        }
     }
 };
