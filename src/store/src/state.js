/**
 * 描述：定义states
 */

export default {
  // 全局公共部分
  pageNum: 1,                                 // 当前页
  totalElement: 0,                            // 总数
  pageSize: 10,                               // 分页数
  modalTransition: 'bounceLeft',              // 弹出动画
  currentUserInfo: {},                        // 当前用户信息
  userInfo: {},                               // 官网用户信息
  companyUserInfo: {},                        // 企业用户信息
  currentCompanyInfo: {},                     // 当前企业信息
  mgeUserInfo: {},                            // 管理员用户信息
  menuTreeList: [],                           // 菜单
  sidebar_an: false,                          // 控制导航
  sysInfoList: {                           // 系统消息个数
    userSysCount: 0
  }
};
