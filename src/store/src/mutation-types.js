/**
 *  描述：定义types
 */

export const CURRENT_USER_INFO = 'CURRENT_USER_INFO';       // 当前用户信息
export const USER_INFO = 'USER_INFO';                       // 官网用户信息
export const COMPANY_USER_INFO = 'COMPANY_USER_INFO';       // 企业用户信息
export const CURRENT_COMPANY_INFO = 'CURRENT_COMPANY_INFO'; // 当前企业信息
export const MGE_USER_INFO = 'MGE_USER_INFO';               // 管理员用户信息
export const MENU_TREE = 'MENU_TREE';                       // 菜单
export const SIDEBAR_AN = 'SIDEBAR_AN';                     // 控制导航
export const SYS_INFO_LIST = 'SYS_INFO_LIST';                     // 系统消息个数
