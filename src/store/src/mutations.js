/**
 *  描述：定义mutations
 */
// import tool from '@/js/core/utils/tool.js';

// 导入 types
import {
  SIDEBAR_AN,
  CURRENT_USER_INFO,
  USER_INFO,
  COMPANY_USER_INFO,
  CURRENT_COMPANY_INFO,
  MGE_USER_INFO,
	MENU_TREE,
  SYS_INFO_LIST
} from './mutation-types';

export default {
  // 控制导航
  [SIDEBAR_AN] (state, info) {
    state.sidebar_an = info.state;
  },
  // 保存当前用户信息
  [CURRENT_USER_INFO] (state, info) {
    state.currentUserInfo = info;
  },
  // 保存官网用户信息
	[USER_INFO] (state, info) {
		state.userInfo = info;
	},
  // 保存企业用户信息
  [COMPANY_USER_INFO] (state, info) {
    state.companyUserInfo = info;
  },
  // 保存当前企业信息
  [CURRENT_COMPANY_INFO] (state, info) {
    state.currentCompanyInfo = info;
  },
  // 保存管理员用户信息
  [MGE_USER_INFO] (state, info) {
    state.mgeUserInfo = info;
  },
	// 获取菜单
	[MENU_TREE] (state, info) {
		state.menuTreeList = info;
	},
  // 系统消息个数
	[SYS_INFO_LIST] (state, info) {
		state.sysInfoList = info;
	}
};
