/**
 *  描述：定义actions
 */

// 导入 types
import {
  CURRENT_USER_INFO, //  当前用户信息
  MGE_USER_INFO, // 管理员用户信息
  COMPANY_USER_INFO,  // 企业用户信息
  CURRENT_COMPANY_INFO,  // 当前企业信息
  USER_INFO,  // 官网用户信息
  SYS_INFO_LIST  // 官网用户信息
} from './mutation-types';

import { managerProfile } from '@/service/mge/login/loginMService';
import { getProfile } from '@/service/company/myAccount/myAccountMService';
import { userList } from '@/service/web/user/userService';
import { getCurrentComp } from '@/service/company/enterpriseInfo/enterpriseInfoMService';
import { systemInfoCount } from '@/service/web/user/systemInfoService';

export default {
  // 获取管理员用户信息
  async getMgeUserInfo ({
                       commit
                     }) {
    let result = await managerProfile();
    commit(MGE_USER_INFO, result.data);
  },
  // 获取企业用户信息
  async getCompanyUserInfo ({
                          commit
                        }) {
    let result = await getProfile();
    commit(COMPANY_USER_INFO, result.data);
    commit(CURRENT_USER_INFO, result.data);
  },
  // 当前企业信息
  async getCurrentComp ({
                              commit
                            }) {
    let result = await getCurrentComp();
    commit(CURRENT_COMPANY_INFO, result.data);
  },
  // 获取官网用户信息
  async getUserInfo ({
                          commit
                        }) {
    let result = await userList();
    commit(USER_INFO, result.data);
    commit(CURRENT_USER_INFO, result.data);
  },
  // 系统消息个数
  async getSysInfoCount ({
                          commit
                        }) {
    let sysInfoList = {};
    let result = await systemInfoCount();
    sysInfoList.userSysCount = result.data || 0;
    commit(SYS_INFO_LIST, sysInfoList);
  }
};
