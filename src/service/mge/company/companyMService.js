/**
 * 描述：运维 - 企业申请记录管理
 */
import { fetch, apiFormat } from '@/service/baseService';

const listApi = '/api/sys/company/{id}/approval';                               // 列表            method 'get'
const countApi = '/api/sys/company/{id}/approval/count';                        // 总数            method 'get'
const approvalApi = '/api/sys/company/approval';                                // 企业审核            method 'post'

// 列表
export const queryList = (payload) => fetch.get(listApi, {params: payload});

// 总数
export const queryCount = (payload) => fetch.get(countApi, {params: payload});

// 企业审核
export const approval = (payload) => fetch.post(approvalApi, payload);
