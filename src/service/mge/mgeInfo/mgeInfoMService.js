/**
 * 描述：运维 - 管理员个人管理
 */
import { fetch, apiFormat } from '@/service/baseService';

const changePasswordApi = '/api/sys/manager/changePassword';                  // 使用原密码更改密码            method 'post'
const changePhoneApi = '/api/sys/manager/changePhone';                        //更换手机号                     method 'post'
const profileApi = '/api/sys/manager/profile';                                // 更新用户信息                  method 'post'

// 列表
export const changePassword = (payload) => fetch.get(changePasswordApi, {params: payload});

// 总数
export const changePhone = (payload) => fetch.get(changePhoneApi, {params: payload});

// 企业审核
export const profile = (payload) => fetch.post(profileApi, payload);
