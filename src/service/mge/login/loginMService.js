/**
 * 描述：后台管理 - 登录
 */
import { fetch, apiFormat } from '@/service/baseService';

const managerLoginApi = '/api/pub/manager/login';                      // 登录                    method 'POST'
const managerProfileApi = '/api/sys/manager/profile';                  // 当前用户信息            method 'GET'

// 登录
export const managerLogin = (payload) => fetch.post(managerLoginApi, null, {params: payload}, {type: 'url'});

// 当前用户信息
export const managerProfile = () => fetch.get(managerProfileApi);
