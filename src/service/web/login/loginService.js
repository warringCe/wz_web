/**
 * 描述：登录service
 */
import { fetch, apiFormat2, apiFormat } from '@/service/baseService';

// login
const loginApi = '/api/pub/user/login';                                    // 系统登陆           method 'GET'

// register
const registerApi = '/api/pub/user/register';                                // 系统注册           method 'GET'

// loginOut
const loginOutApi = '/api/pub/user/logout';                                // 系统注销           method 'GET'

// userInfo
const getUserInfoApi = '/api/sys/admin/loginInfo';                          // 获取用户信息       method 'GET'

// captcha
const getCaptchaPicApi = '/api/pub/captcha.jpg';                          // 获取验证码图片       method 'GET'

// captcha
const getSendSmsCodeApi = '/api/pub/sendSmsCode/{phone}';                          // 发送短信验证码(需要图片验证码)       method 'GET'

// 登录
export const login = (payload) => fetch.post(apiFormat2(loginApi, payload), payload, {type: 'url'});

// 获取用户信息
export const register = (payload) => fetch.post(apiFormat2(registerApi, payload), payload);

// 注销
export const loginOut = () => fetch.get(loginOutApi);

// 获取用户信息
export const getUserInfo = () => fetch.get(getUserInfoApi);

// 获取验证码图片
export const captchaPic = (opt) => fetch.get(getCaptchaPicApi, {params: opt});

// 发送手机验证码

export const sendSmsCode = (opt, payload) => fetch.post(apiFormat(getSendSmsCodeApi, opt), null, {params: payload});
