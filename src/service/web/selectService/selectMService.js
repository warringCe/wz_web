/**
 * 描述：企业 - 下拉框集合
 */
import { fetch, apiFormat } from '@/service/baseService';

const districtApi = '/api/pub/district';                               //省市区查询                      method 'get'
const districtByCityIdApi = '/api/pub/district/{cityId}';              //查询某城市下区县列表            method 'get'
// 字典表查询
const industryApi = '/api/pub/dict/industry';            // 查询行业字典              method 'get'
const dictJopApi = '/api/pub/dict/job';                  // 查询岗位字典              method 'get'
const dictJopSearchApi = '/api/pub/dict/job/search';     // 查询所有岗位字典          method 'get'
const companyScaleApi = '/api/pub/dict/companyScale';     // 查询企业规模字典          method 'get'
const capitalStageApi = '/api/pub/dict/capitalStage';     // 查询企业融资阶段字典          method 'get'

// 省市区查询
export const district = (para) => fetch.get(districtApi, {params: para});

// 查询某城市下区县列表
export const districtByCityId = (opt) => fetch.get(apiFormat(districtByCityIdApi, opt));

// 字典表查询
export const industry = () => fetch.get(industryApi);  // 行业字典表
export const dictJob = () => fetch.get(dictJopApi); // 岗位字典表
export const dictJopSearch = () => fetch.get(dictJopSearchApi); // 查询所有岗位字典
export const companyScale = () => fetch.get(companyScaleApi); // 查询企业规模字典
export const capitalStage = () => fetch.get(capitalStageApi); // 查询企业融资阶段字典
