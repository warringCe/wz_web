/**
 * 描述：详情service
 */
import { fetch, apiFormat } from '@/service/baseService';

const detailCompanyApi = '/api/pub/company/{id}';                // 企业详情           method 'GET'
const detailJobApi = '/api/pub/company/job/{id}';                // 职务详情           method 'GET'
const submitApplApi = '/api/user/appl';                          // 提交申请           method 'post'

// 企业详情
export const detailCompany = (opt) => fetch.get(apiFormat(detailCompanyApi, opt));

// 职务详情
export const detailJob = (opt) => fetch.get(apiFormat(detailJobApi, opt));

// 提交申请
export const submitAppl = (para) => fetch.post(submitApplApi, null, {params: para});
