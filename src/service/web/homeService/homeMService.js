/**
 * 描述：企业 - 下拉框集合
 */
import { fetch, apiFormat } from '@/service/baseService';

const getCompanyListApi = '/api/pub/company';              //查询企业                    method 'get'
const getJobListApi = '/api/pub/company/job';              //查询职位信息列表            method 'get'

// 查询企业
export const getCompanyList = (para) => fetch.get(getCompanyListApi, {params: para});

// 查询职位信息列表
export const getCompanyJobList = (para) => fetch.get(getJobListApi, {params: para});
