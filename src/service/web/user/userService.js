/**
 * 描述：员工管理service
 */
import { fetch, apiFormat } from '@/service/baseService';

const getUserListApi = '/api/user/profile';                      // 获取当前用户信息           method 'GET'
const modifyUserApi = '/api/user/profile';                      // 更新当前用户信息           method 'POST'
const changePasswordApi = '/api/user/changePassword';                      // 使用原密码更改密码           method 'POST'
const changePhoneApi = '/api/user/changePhone';                    // 更换手机号            method 'POST'
const getResumeAndIntentApi = '/api/user/resumeAndIntent';                    // 获取简历及求职意向信息           method 'GET'

const uploadImageApi = '/api/user/image';                    // 上传头像或Logo，返回JSON中的ossKey字段为保存的文件名            method 'POST'
// 求职意向
const getIntentApi = '/api/user/intent';                    // 查看求职意向           method 'GET'
const uploadIntentApi = '/api/user/intent';                    // 更新求职意向            method 'POST'
const getProfileAndIntentApi = '/api/user/profileAndIntent';                    // 查看基本信息与求职意向    弃用        method 'POST' 超哥说会删除
// 教育经历
const getEducationApi = '/api/user/education';                    // 查看教育经历           method 'GET'
const uploadEducationApi = '/api/user/education/{id}';                    // 更新教育经历            method 'POST'
const addEducationApi = '/api/user/education';                    // 新增教育经历            method 'POST'
const deleteEducationApi = '/api/user/education/{id}';                    // 删除教育经历            method 'POST'
// 工作经历
const resumeUpdateApi = '/api/user/resume/{id}';            // 更新工作经历            method 'POST'
const resumeAddApi = '/api/user/resume';                    // 新增工作经历            method 'POST'
const resumeDeleteApi = '/api/user/resume/{id}';             // 删除工作经历            method 'POST'
// 上传简历
const resumeFileListApi = '/api/user/resume/attachment';               // 查看简历附件列表                    method 'get'
const resumeFileAddApi = '/api/user/resume/upload';                    // 新增简历                            method 'POST'
const resumeFileUpdateApi = '/api/user/resume/attachment/{id}';        // 修改简历附件的显示文件名            method 'POST'
const resumeFiledeleteApi = '/api/user/resume/attachment/{id}';        // 修改简历附件的显示文件名            method 'delete'
const visibleApi = '/api/user/visible';        // 修改简历状态            method 'delete'

// 简历完整度
const profileCompletenessApi = '/api/user/profileCompleteness';        // 简历完整度            method 'delete'

// 获取当前用户信息
export const userList = (opt) => fetch.get(getUserListApi, {params: opt});
// 更新当前用户信息
export const modifyUser = (payload) => fetch.post(modifyUserApi, payload);
// 使用原密码更改密码
export const changePassword = (para) => fetch.post(changePasswordApi, null, {params: para});
// 更换手机号
export const changePhone = (payload) => fetch.post(changePhoneApi, null, {params: payload});
// 获取简历及求职意向信息
export const getResumeAndIntent = (payload) => fetch.get(getResumeAndIntentApi, payload);
// 上传头像或Logo
export const uploadImage = (payload) => fetch.post(uploadImageApi, payload);

// 求职意向
export const getIntent = (payload) => fetch.get(getIntentApi, payload);
// 求职意向 - 修改
export const uploadIntent = (payload) => fetch.post(uploadIntentApi, payload);
// 查看基本信息与求职意向
export const getProfileAndIntent = (payload) => fetch.get(getProfileAndIntentApi, payload);

// 教育经历
export const getEducation = (payload) => fetch.get(getEducationApi, payload);
// 教育经历 - 修改
export const uploadEducation = (opt, payload) => fetch.post(apiFormat(uploadEducationApi, opt), payload);
// 教育经历 - 新增
export const addEducation = (payload) => fetch.post(addEducationApi, payload);
// 教育经历 - 删除
export const deleteEducation = (opt) => fetch.delete(apiFormat(deleteEducationApi, opt));

// 工作经历 - 修改
export const resumeUpdate = (opt, payload) => fetch.post(apiFormat(resumeUpdateApi, opt), payload);
// 工作经历 - 新增
export const resumeAdd = (payload) => fetch.post(resumeAddApi, payload);
// 工作经历 - 删除
export const resumeDelete = (opt) => fetch.delete(apiFormat(resumeDeleteApi, opt));

// 查看简历附件列表
export const resumeFileList = () => fetch.get(resumeFileListApi);
// 简历附件 - 新增
export const resumeFileAdd = (para) => fetch.post(resumeFileAddApi, para);
// 简历附件 - 修改名称
export const resumeFileUpdate = (opt, para) => fetch.post(apiFormat(resumeFileUpdateApi, opt), null, {params: para});
// 简历附件 - 删除
export const resumeFiledelete = (opt) => fetch.delete(apiFormat(resumeFiledeleteApi, opt));

// 简历 - 修改状态
export const visibleFun = (para) => fetch.post(visibleApi, null , {params: para});

// 简历完整度
export const profileCompleteness = () => fetch.get(profileCompletenessApi);
