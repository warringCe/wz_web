/**
 * 描述：系统消息service
 */
import { fetch, apiFormat } from '@/service/baseService';

const systemInfoListApi = '/api/user/notice';                      // 系统消息          method 'GET'
const systemInfoCountApi = '/api/user/notice/count';               // 系统消息数量           method 'GET'
const noticeAckApi = '/api/user/notice/ack/{id}';               // 系统消息已读           method 'GET'

// 系统消息
export const systemInfoList = (opt) => fetch.get(systemInfoListApi, {params: opt});
// 系统消息数量
export const systemInfoCount = () => fetch.get(systemInfoCountApi);
// 系统消息已读
export const noticeAck = (opt) => fetch.get(apiFormat(noticeAckApi, opt));
