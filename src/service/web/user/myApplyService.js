/**
 * 描述：员工管理service
 */
import { fetch, apiFormat } from '@/service/baseService';

const listApi = '/api/user/appl';                      // 列表                   method 'GET'
const countApi = '/api/user/appl/count';               // 总数                   method 'get'
const acceptApi = '/api/user/appl';        //接受职位邀请            method 'post'

// 列表
export const queryList = (payload) => fetch.get(listApi, {params: payload});

// 总数
export const queryCount = (payload) => fetch.get(countApi, {params: payload});

// 接受职位邀请
export const acceptFun = (opt) => fetch.post(apiFormat(acceptApi, opt));
