/**
 * 描述：员工管理service
 */
import { fetch, apiFormat } from '@/service/baseService';

const visitorListApi = '/api/user/visitor';                      // 看过我的招聘员列表           method 'GET'
const visitorCountApi = '/api/user/visitor/count';                      // 看过我的招聘员数量           method 'GET'

// 看过我的招聘员列表
export const visitorList = (opt) => fetch.get(visitorListApi, {params: opt});
// 看过我的招聘员数量
export const visitorCount = () => fetch.get(visitorCountApi);

