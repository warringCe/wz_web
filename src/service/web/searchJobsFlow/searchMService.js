/**
 * 描述：菜单管理service
 */
import { fetch } from '@/service/baseService';

const jobListApi = '/api/pub/company/job';                       // 列表           method 'GET'
const jobCountApi = '/api/pub/company/job/count';                // 总数           method 'GET'

// 列表
export const queryList = (para) => fetch.get(jobListApi, {params: para});

// 总数
export const queryCount = (para) => fetch.get(jobCountApi, {params: para});
