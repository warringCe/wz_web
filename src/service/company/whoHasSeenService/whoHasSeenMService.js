/**
 * 描述：企业 - 职位管理
 */
import { fetch, apiFormat } from '@/service/baseService';

const listApi = '/api/user/visitor';                               // 列表            method 'get'
const countApi = '/api/user/visitor/count';                        // 总数            method 'get'

// 列表
export const queryList = (payload) => fetch.get(listApi, {params: payload});

// 总数
export const queryCount = (payload) => fetch.get(countApi, {params: payload});
