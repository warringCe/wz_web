/**
 * 描述：企业 - 企业账户管理
 */
import { fetch, apiFormat } from '@/service/baseService';

const listApi = '/api/compAdmin/staff';                               // 列表            method 'get'
const countApi = '/api/compAdmin/staff/count';                        // 总数            method 'get'
const addApi = '/api/compAdmin/staff';                                // 新增            method 'post'
const updateApi = '/api/compAdmin/staff/{id}';                        // 修改            method 'post'
const deleteApi = '/api/compAdmin/staff/{id}';                        // 删除            method 'delete'
const changePhoneApi = '/api/compAdmin/staff/{id}/changePhone';       // 修改某个账户手机号            method 'post'
const banApi = '/api/compAdmin/staff/ban/{id}';                       // 禁用账号            method 'post'
const unbanApi = '/api/compAdmin/staff/unban/{id}';                   // 解禁帐号            method 'post'

// 列表
export const queryList = (payload) => fetch.get(listApi, {params: payload});

// 总数
export const queryCount = (payload) => fetch.get(countApi, {params: payload});

// 新增
export const addFun = (payload) => fetch.post(addApi, payload);

// 修改
export const updateFun = (opt, payload) => fetch.post(apiFormat(updateApi, opt), payload);

// 删除
export const deleteFun = (opt, payload) => fetch.delete(apiFormat(deleteApi, opt), payload);

// 修改某个账户手机号
export const changePhone = (opt, para) => fetch.post(apiFormat(changePhoneApi, opt), null, {params: para});

// 禁用账号
export const banFun = (opt, payload) => fetch.post(apiFormat(banApi, opt), payload);

// 解禁帐号
export const unbanFun = (opt, payload) => fetch.post(apiFormat(unbanApi, opt), payload);
