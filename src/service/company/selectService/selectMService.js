/**
 * 描述：企业 - 下拉框集合
 */
import { fetch, apiFormat } from '@/service/baseService';

const districtApi = '/api/pub/district';                              //省市区查询                      method 'get'
const districtByCityIdApi = '/api/pub/district/{cityId}';             //查询某城市下区县列表            method 'get'
const provinceAndCityApi = '/api/pub/district/provinceAndCity';       //获取全部省市数据                method 'get'
const capitalStageApi = '/api/pub/dict/capitalStage';              //查询企业融资阶段字典            method 'get'
const companyScaleApi = '/api/pub/dict/companyScale';              //查询企业规模字典                method 'get'
const industryApi = '/api/pub/dict/industry';            // 查询行业字典              method 'get'
const dictJobApi = '/api/pub/dict/job';                  // 查询岗位字典              method 'get'
const dictJobSearchApi = '/api/pub/dict/job/search';     // 按岗位名称搜索岗位        method 'get'

// 省市区查询
export const district = (para) => fetch.get(districtApi, {params: para});

// 查询某城市下区县列表
export const districtByCityId = (opt) => fetch.get(apiFormat(districtByCityIdApi, opt));

// 获取全部省市数据
export const provinceAndCity = (opt) => fetch.get(provinceAndCityApi);

// 查询企业融资阶段字典
export const capitalStage = (para) => fetch.get(capitalStageApi);

// 查询企业规模字典
export const companyScale = (para) => fetch.get(companyScaleApi);

// 查询行业字典
export const industry = () => fetch.get(industryApi);

// 查询岗位字典
export const dictJob = () => fetch.get(dictJobApi);

// 按岗位名称搜索岗位
export const dictJobSearch = (para) => fetch.get(dictJobSearchApi, {params: para});
