/**
 * 描述：企业 - 简历管理
 */
import { fetch, apiFormat } from '@/service/baseService';

const listApi = '/api/comp/talent';                      // 列表                              method 'get'
const countApi = '/api/comp/talent/count';               // 总数                              method 'get'
const byIdApi = '/api/comp/talent/{id}';                 // 查看人才求职意向及简历            method 'get'
const jobListApi = '/api/comp/job';                      // 职位列表                          method 'get'

// 列表
export const queryList = (payload) => fetch.get(listApi, {params: payload});

// 总数
export const queryCount = (payload) => fetch.get(countApi, {params: payload});

// 查看人才求职意向及简历
export const getById = (opt) => fetch.get(apiFormat(byIdApi, opt));

// 职位列表
export const jobList = (payload) => fetch.get(jobListApi, {params: payload});
