/**
 * 描述：企业 - 职位邀请
 */
import { fetch, apiFormat } from '@/service/baseService';

const listApi = '/api/comp/invite';                      // 列表            method 'get'
const countApi = '/api/comp/invite/count';               // 总数            method 'get'
const inviteApi = '/api/comp/invite';                                            // 向发出职位邀请            method 'post'
const changeStatusApi = '/api/comp/invite/changeStatus/{id}';                    // 更新职位邀请状态            method 'post'

// 列表
export const queryList = (para) => fetch.get(listApi, {params: para});

// 总数
export const queryCount = (para) => fetch.get(countApi, {params: para});

// 向发出职位邀请
export const invite = (para) => fetch.post(inviteApi, null, {params: para});

// 更新职位邀请状态
export const changeStatus = (opt, para) => fetch.post(apiFormat(changeStatusApi, opt), null, {params: para});
