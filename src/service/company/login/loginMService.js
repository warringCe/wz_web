/**
 * 描述：企业管理 - 登录
 */
import { fetch, apiFormat } from '@/service/baseService';

const companyLoginApi = '/api/pub/staff/login';                      // 登录            method 'POST'
const companyRegisterApi = '/api/pub/company/register';              // 注册            method 'POST'
const companyLoginForgotApi = '/api/pub/staff/changePasswordBySms';                      // 忘记密码            method 'POST'
const getSendSmsCodeApi = '/api/pub/sendSmsCode/{phone}';                          // 发送短信验证码(需要图片验证码)       method 'GET'

// 登录
export const companyLogin = (payload) => fetch.post(companyLoginApi, null, {params: payload}, {type: 'url'});

// 注册
export const companyRegister = (payload) => fetch.post(companyRegisterApi, payload);
// 注册
export const companyLoginForgot = (payload) => fetch.post(companyLoginForgotApi, null, {params: payload}, {type: 'url'});
// 发送手机验证码

export const sendSmsCode = (opt, payload) => fetch.post(apiFormat(getSendSmsCodeApi, opt), null, {params: payload});
