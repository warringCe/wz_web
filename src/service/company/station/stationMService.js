/**
 * 描述：企业 - 职位管理
 */
import { fetch, apiFormat } from '@/service/baseService';

const listApi = '/api/comp/job';                               // 列表            method 'get'
const countApi = '/api/comp/job/count';                        // 总数            method 'get'
const getByIdApi = '/api/comp/job/{id}';                       // 详情            method 'get'
const addApi = '/api/comp/job';                                // 新增            method 'post'
const updateApi = '/api/comp/job/{id}';                        // 修改            method 'post'
const publishApi = '/api/comp/job/publish/{id}';               // 发布            method 'post'
const offlineApi = '/api/comp/job/offline/{id}';               // 下线            method 'post'
const delFunApi = '/api/comp/job/{id}';                        // 删除            method 'delete'

// 列表
export const queryList = (payload) => fetch.get(listApi, {params: payload});

// 总数
export const queryCount = (payload) => fetch.get(countApi, {params: payload});

// 详情
export const getDetail = (opt) => fetch.get(apiFormat(getByIdApi, opt));

// 新增
export const addFun = (payload) => fetch.post(addApi, payload);

// 修改
export const updateFun = (opt, payload) => fetch.post(apiFormat(updateApi, opt), payload);

// 发布
export const publish = (opt, payload) => fetch.post(apiFormat(publishApi, opt), payload);

// 下线
export const offline = (opt, payload) => fetch.post(apiFormat(offlineApi, opt), payload);

// 删除
export const delFun = (opt) => fetch.delete(apiFormat(delFunApi, opt));
