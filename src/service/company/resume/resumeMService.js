/**
 * 描述：企业 - 简历管理
 */
import { fetch, apiFormat } from '@/service/baseService';

const listApi = '/api/comp/appl';                             // 列表            method 'get'
const countApi = '/api/comp/appl/count';                      // 总数            method 'get'
const getByIdApi = '/api/comp/appl/{id}';                     // 详情            method 'get'
const attachmentApi = '/api/comp/appl/{id}/attachment';       // 职位申请的附件简历链接地址            method 'get'
const changeStatusApi = '/api/comp/appl/changeStatus/{id}';   // 更改职位申请状态            method 'get'

// 列表
export const queryList = (payload) => fetch.get(listApi, {params: payload});

// 总数
export const queryCount = (payload) => fetch.get(countApi, {params: payload});

// 详情
export const getById = (opt) => fetch.get(apiFormat(getByIdApi, opt));

// 职位申请的附件简历链接地址
export const attachment = (opt) => fetch.get(apiFormat(attachmentApi, opt));

// 更改职位申请状态
export const changeStatus = (opt, para) => fetch.post(apiFormat(changeStatusApi, opt), null, {params: para});
