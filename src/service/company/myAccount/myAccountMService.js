/**
 * 描述：企业 - 企业管理
 */
import { fetch, apiFormat } from '@/service/baseService';

const changePasswordApi = '/api/comp/staff/changePassword';                     // 使用原密码更改密码    method 'post'
const getProfileApi = '/api/comp/staff/profile';                                // 当前账户信息      method 'get'
const updateProfileApi = '/api/comp/staff/profile';                             // 更新当前账户信息      method 'post'
const changePhoneApi = '/api/comp/staff/changePhone';                           // 更换手机号            method 'post'
const noticeCountApi = '/api/comp/notice/count';                                // 查询通知消息条数            method 'get'

// 使用原密码更改密码
export const changePassword = (payload) => fetch.post(changePasswordApi, null, {params: payload});

// 当前账户信息
export const getProfile = () => fetch.get(getProfileApi);

// 跟新当前账户信息
export const updateProfile = (payload) => fetch.post(updateProfileApi, payload);

// 更换手机号
export const changePhone = (para) => fetch.post(changePhoneApi, null, {params: para});

// 查询通知消息条数
export const noticeCount = () => fetch.get(noticeCountApi);
