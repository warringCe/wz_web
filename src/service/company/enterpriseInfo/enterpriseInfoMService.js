/**
 * 描述：企业 - 企业管理
 */
import { fetch, apiFormat } from '@/service/baseService';

const listApi = '/api/compAdmin/approval';                      // 列表            method 'get'
const countApi = '/api/compAdmin/approval/count';               // 总数            method 'get'
const updateApi = '/api/compAdmin/profile';                      // 修改            method 'post'
const getCompanyInfoApi = '/api/compAdmin';                             // 获取企业信息    method 'get'
const submitApprovalApi = '/api/compAdmin/submitApproval';              // 提交企业审核    method 'post'
const industryApi = '/api/pub/dict/industry';            // 查询行业字典              method 'get'
const getCurrentCompApi = '/api/compAdmin/profile';      // 当前企业信息              method 'get'

// 列表
export const queryList = (payload) => fetch.get(listApi, {params: payload});

// 总数
export const queryCount = (payload) => fetch.get(countApi, {params: payload});

// 修改
export const updateFun = (opt, payload) => fetch.post(updateApi, payload);

// 获取企业信息
export const getCompanyInfo = () => fetch.get(getCompanyInfoApi);

// 提交企业审核
export const submitApproval = () => fetch.post(submitApprovalApi);

// 查询行业字典
export const industry = () => fetch.get(industryApi);

// 当前企业信息
export const getCurrentComp = () => fetch.get(getCurrentCompApi);
